import csv
import random

import functions.functions as af
import random
"""
    Création de groupe à partir de la promotion
    version: 1.0
    date : 06/12/2023
    Auteur : Loan Heniart
"""



pathFile = "./file/promotion_B3_B.csv"

with open(pathFile, 'r') as file:
    next(file)
    students = [f"{fields[1]} {fields[0]}" for line in file for fields in [line.split(";")]]
    random.shuffle(students)
    nom_fichier = "./file/groupes.csv"
    paires = af.computeGroupe(students)
    try:
        with open(nom_fichier, 'w', newline='') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerow(['Groupe', 'Membre 1', 'Membre 2'])  # Écrire l'en-tête du fichier CSV

            for index, paire in enumerate(paires, 1):
                print(f"Groupe:{index}, {paire[0]}, {paire[1]}")
                csvwriter.writerow([index, paire[0], paire[1]])

        print(f"Les résultats ont été enregistrés dans {nom_fichier}")
    except Exception as e:
        print(f"Erreur lors de l'enregistrement du fichier : {e}")

