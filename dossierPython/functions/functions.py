
def computeGroupe(prmTab):
    """
    Creation des groupes avec 2 étudiants
    :param prmTab: tableau d'étudiants
    :return:  tableau de groupe
    """
    if len(prmTab) % 2 != 0:
        prmTab.append("")
    groupe = list(zip(prmTab[::2], prmTab[1::2]))
    return groupe

if __name__ == "__main__":
    print("Exécution directe du script.")