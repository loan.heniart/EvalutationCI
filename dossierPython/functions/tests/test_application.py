import pytest
import functions.functions as af

def test_computeGroupe():
    """
        Test unitaire computeGroupe
    """
    tab = []
    for i in range(4):
        tab.append(f"Prenom{i} Nom{i}")
    result = af.computeGroupe(tab)
    assert result[0] == ('Prenom0 Nom0', 'Prenom1 Nom1')
    tab = ["Prenom0 Nom0"]
    result = af.computeGroupe(tab)
    assert result[0] == ('Prenom0 Nom0', '')



