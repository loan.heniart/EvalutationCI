@echo off
REM sapache.bat
REM Usage: sapache.bat start|stop|create

SET CONTAINER_NAME=apache_container
SET IMAGE_NAME=httpd
SET HOST_PORT=80

IF "%1"=="start" (
    echo Starting container...
    docker run -d --name %CONTAINER_NAME% -p %HOST_PORT%:80 %IMAGE_NAME%
) ELSE IF "%1"=="stop" (
    echo Stopping container...
    for /f "tokens=*" %%i in ('docker ps -q --filter "name=%CONTAINER_NAME%"') do docker stop %%i
) ELSE (
    echo Invalid argument: %1
    echo Usage: sapache.bat start|stop
)