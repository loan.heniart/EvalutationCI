@echo off
REM zipper.bat
REM Usage: zipper.bat [zip_file_name] [directory_to_zip]

SET zip_file_name=%1
SET directory_to_zip=%2

REM Check if 7zip is installed
IF NOT EXIST "C:\Program Files\7-Zip\7z.exe" (
    echo 7zip not found. Please install it from https://www.7-zip.org/
    exit /b 1
)

REM Zip specific files
"C:\Program Files\7-Zip\7z.exe" a -tzip "%zip_file_name%" ^
    "%directory_to_zip%\file" ^
    "%directory_to_zip%\functions" ^
    "%directory_to_zip%\groupePromotion.py"

echo Specified files have been zipped into "%zip_file_name%"